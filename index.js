const Discord = require('discord.js');
const client = new Discord.Client();

const fs = require("fs")
var reactionconfig = JSON.parse(fs.readFileSync("reaction_settings.json"))
const guildid = reactionconfig.guildid

const events = {
    MESSAGE_REACTION_ADD: 'messageReactionAdd',
    MESSAGE_REACTION_REMOVE: 'messageReactionRemove',
};

client.on('raw', async event => {
    if (!events.hasOwnProperty(event.t)) return;

    const {
        d: data
    } = event;
    const user = client.users.get(data.user_id);
    const channel = client.channels.get(data.channel_id) || await user.createDM();

    let message = await channel.fetchMessage(data.message_id);

    const emojiKey = (data.emoji.id) ? `${data.emoji.name}:${data.emoji.id}` : data.emoji.name;

    let reaction = message.reactions.get(emojiKey);

    if (!reaction) {
        const emoji = new Discord.Emoji(client.guilds.get(data.guild_id), data.emoji);
        reaction = new Discord.MessageReaction(message, emoji, 1, data.user_id === client.user.id);
    }

    client.emit(events[event.t], reaction, user);

});

client.on('messageReactionAdd', (reaction, user) => {


    if (user.bot) return;

    if (user.joinstats != 2) return;

    if (reaction.message.id != user.joinmessageid) return;

    for (var index in reactionconfig.settings) {
        if (reactionconfig.settings[index].emote == reaction.emoji.name) {
            var guild = client.guilds.get(guildid)
            var role = guild.roles.find(role => role.name === reactionconfig.settings[index].role)
            var member = guild.members.get(user.id);
            member.addRole(role)
            break
        }
    }

});

client.on('messageReactionRemove', (reaction, user, message) => {


    if (user.bot) return;

    if (user.joinstats != 2) return;

    if (reaction.message.id != user.joinmessageid) return;

    for (var index in reactionconfig.settings) {
        if (reactionconfig.settings[index].emote == reaction.emoji.name) {
            var guild = client.guilds.get(guildid)
            var role = guild.roles.find(role => role.name === reactionconfig.settings[index].role)
            var member = guild.members.get(user.id);
            member.removeRole(role)
            break
        }
    }
});

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', (msg) => {
    if (msg.author.bot) return;
    if (msg.channel.type == "dm") {
        if (msg.author.joinstats == 2) {
            if (msg.content.toLowerCase().trim() == "fini") {
                msg.author.send(new Discord.RichEmbed()
                    .setColor("#00ff00")
                    .setTitle("Fini !")
                    .setDescription("Tu peux à présent profiter pleinement du Discord.\nSi l'envie te prends de changer de role, écrit la commande `!role`")
                )
                msg.author.joinstats = 0
            }
        }
        if (msg.author.joinstats == 1) {
            var args = msg.content.trim().split(" ")
            if (!args[0] || !args[1]) {
                msg.channel.send(new Discord.RichEmbed()
                    .setTitle(":warning: ERREUR :warning:")
                    .setColor("#ff0000")
                    .setDescription("Ton nom RP est invalide.\nMerci d'en choisir un avec cette syntaxe : `Nom Prénom`"))
                return
            }
            var guild = client.guilds.get(guildid)
            var memb = guild.members.get(msg.author.id)
            memb.setNickname(args[0] + " " + args[1])

            // var index in reactionconfig.settings
            var embed = new Discord.RichEmbed()
                .setTitle("Gestion des roles")
                .setColor("#fcdb00")
                .setDescription("Notre communauté dispose de plusieurs serveurs. Pour avoir accès aux éléments concernant ces derniers, réagit avec les réactions juste en dessous de ce message.\n> Une fois cela fait, tappe `Fini`.")

            for (var index in reactionconfig.settings) {
                embed.addField(reactionconfig.settings[index].server, reactionconfig.settings[index].emote, true)
            }
            msg.author.send(embed)
                .then((message) => {
                    msg.author.joinstats = 2
                    msg.author.joinmessageid = message.id
                    for (var index in reactionconfig.settings) {
                        message.react(reactionconfig.settings[index].emote)
                    }
                })
        }
    }
    if (msg.content.startsWith("!role")) {
        msg.delete()
        msg.author.createDM()

        var embed = new Discord.RichEmbed()
            .setTitle("Gestion des roles")
            .setColor("#fcdb00")
            .setDescription("Notre communauté dispose de plusieurs serveurs. Pour avoir accès aux éléments concernant ces derniers, réagit avec les réactions juste en dessous de ce message.\n> Une fois cela fait, tappe `Fini`")

        for (var index in reactionconfig.settings) {
            embed.addField(reactionconfig.settings[index].server, reactionconfig.settings[index].emote, true)
        }
        msg.author.send(embed)
            .then((message) => {
                msg.author.joinstats = 2
                msg.author.joinmessageid = message.id
                for (var index in reactionconfig.settings) {
                    message.react(reactionconfig.settings[index].emote)
                }
            })
    }

    if (msg.content.startsWith("!join")) {
        msg.delete()
        msg.author.joinstats = 1
        msg.author.createDM()
        msg.author.send(new Discord.RichEmbed().setTitle("Bienvenue sur le Discord de Symphony")
            .setColor("#fcdb00")
            .setDescription("Pour commencer, quel est ton nom RolePlay en jeu (Nom Prénom) ? "))
    }
})

function getMessageByID(channel, id) {
    channel.fetchMessage(id).then(function (r) {
        return r
    })
}

client.login('NjA3NTMyMTM3MjA5NTI4MzM3.XUcYDw.MD2Kc7W462qPxpuFogPRLeQPQuw');